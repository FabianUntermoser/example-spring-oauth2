package com.example.springoauth2;

import java.security.Principal;

import javax.annotation.security.RolesAllowed;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(produces = {"application/json"})
public class LoginController
{
    @RolesAllowed("USER")
    @RequestMapping("/**")
    public String getUser()
    {
        return "Welcome User";
    }

//    @RolesAllowed({"USER", "ADMIN"})
    @RolesAllowed("ADMIN")
    @RequestMapping("/admin")
    public String getAdmin()
    {
        return "Welcome Admin";
    }

    @RequestMapping(value = "/info", produces = {"application/json"})
    public ResponseEntity<Principal> getPrincipal(Principal principal)
    {
        return ResponseEntity.ok(principal);
    }

}
